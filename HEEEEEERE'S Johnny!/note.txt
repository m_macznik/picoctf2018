In this task we need to find the way to log into given shell.
We need to retrieve password from given files.

With help comes the program unshadow from John the Ripper.
If not present in system type : sudo apt-get install john

Then we can see how this tool works with manual: man unshadow

Usage of this tool: unshadow passwd_file shadow_file. 
We should also get output from this tool to file which will be used later.
unshadow passwd shadow > unshadow.out

Then after the files will combine, we need to see the password for the shell.
john -show unshadow.out

and we have credentials: root:password1:0:0:root:/root:/bin/bash

Log into shell with retrieved credentials
nc 2018shell.picoctf.com 38860

And after successfull login we have the output:
Username: root
Password: password1
picoCTF{J0hn_1$_R1pp3d_4e5aa29e}

Flag: picoCTF{J0hn_1$_R1pp3d_4e5aa29e}